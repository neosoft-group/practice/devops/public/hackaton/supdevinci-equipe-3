apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: app-otel
  namespace: infra-argocd
  labels:
    app.kubernetes.io/name: helm-infra-argocd
    app.kubernetes.io/component: app-otel
    app.kubernetes.io/managed-by: helm-infra-argocd
spec:
  destination:
    namespace: app-otel
    server: https://kubernetes.default.svc
  project: app-otel
  syncPolicy:
    automated: {}
  source:
    repoURL: https://open-telemetry.github.io/opentelemetry-helm-charts
    chart: opentelemetry-collector
    targetRevision: v0.93.3
    helm:
      values: |-
        fullnameOverride: app-otel
        mode: deployment
        image:
          repository: otel/opentelemetry-collector-contrib
        podAnnotations:
          pod-security.kubernetes.io/enforce: privileged
        securityContext:
          runAsUser: 0
          runAsGroup: 0
          capabilities:
            add:
              - SYS_ADMIN
              - NET_ADMIN
        extraVolumes:
          - name: varlog
            hostPath:
              path: /var/log
        extraVolumeMounts:
          - name: varlog
            mountPath: /var/log
            readOnly: true
        clusterRole:
          create: true
          rules:
            - apiGroups:
                - ''
              resources:
                - nodes
                - nodes/proxy
                - services
                - endpoints
                - pods
                - events
                - namespaces
                - namespaces/status
                - pods/status
                - replicationcontrollers
                - replicationcontrollers/status
                - resourcequotas
              verbs:
                - get
                - list
                - watch
            - nonResourceURLs:
                - /metrics
              verbs:
                - get
            - apiGroups:
                - apps
              resources:
                - daemonsets
                - deployments
                - replicasets
                - statefulsets
              verbs:
                - get
                - list
                - watch
            - apiGroups:
                - extensions
              resources:
                - daemonsets
                - deployments
                - replicasets
              verbs:
                - get
                - list
                - watch
            - apiGroups:
                - batch
              resources:
                - jobs
                - cronjobs
              verbs:
                - get
                - list
                - watch
            - apiGroups:
                - autoscaling
              resources:
                - horizontalpodautoscalers
              verbs:
                - get
                - list
                - watch
        config:
          receivers:
            filelog:
              include:
                - /var/log/pods/*/*/*.log
              start_at: beginning
              include_file_path: true
              include_file_name: false
              operators:
                - type: router
                  id: get-format
                  routes:
                    - output: parser-docker
                      expr: 'body matches "^\\{"'
                    - output: parser-crio
                      expr: 'body matches "^[^ Z]+ "'
                    - output: parser-containerd
                      expr: 'body matches "^[^ Z]+Z"'
                - type: regex_parser
                  id: parser-crio
                  regex: '^(?P<time>[^ Z]+) (?P<stream>stdout|stderr) (?P<logtag>[^ ]*) ?(?P<log>.*)$'
                  output: extract_metadata_from_filepath
                  timestamp:
                    parse_from: attributes.time
                    layout_type: gotime
                    layout: '2006-01-02T15:04:05.999999999Z07:00'
                - type: regex_parser
                  id: parser-containerd
                  regex: '^(?P<time>[^ ^Z]+Z) (?P<stream>stdout|stderr) (?P<logtag>[^ ]*) ?(?P<log>.*)$'
                  output: extract_metadata_from_filepath
                  timestamp:
                    parse_from: attributes.time
                    layout: '%Y-%m-%dT%H:%M:%S.%LZ'
                - type: json_parser
                  id: parser-docker
                  output: extract_metadata_from_filepath
                  timestamp:
                    parse_from: attributes.time
                    layout: '%Y-%m-%dT%H:%M:%S.%LZ'
                - type: regex_parser
                  id: extract_metadata_from_filepath
                  regex: '^.*\/(?P<namespace>[^_]+)_(?P<pod_name>[^_]+)_(?P<uid>[a-f0-9\-]{16,36})\/(?P<container_name>[^\._]+)\/(?P<restart_count>\d+)\.log$'
                  parse_from: attributes["log.file.path"]
                  cache:
                    size: 128
                - type: move
                  from: attributes["log.file.path"]
                  to: resource["filename"]
                - type: move
                  from: attributes.container_name
                  to: resource["container"]
                - type: move
                  from: attributes.namespace
                  to: resource["namespace"]
                - type: move
                  from: attributes.pod_name
                  to: resource["pod"]
                - type: add
                  field: resource["cluster"]
                  value: 'eks-cluster'
            k8s_events:
              namespaces: []
          exporters:
            loki:
              endpoint: http://app-loki-distributor.app-loki:3100/loki/api/v1/push
            otlphttp:
              endpoint: http://app-tempo-distributor.app-tempo:4318
              tls:
                insecure: true
            otlp:
              endpoint: http://app-tempo-distributor.app-tempo:4317
              tls:
                insecure: true
          processors:
            resource/k8s_events:
              attributes:
                - action: insert
                  key: cluster
                  value: 'eks-cluster'
                - action: insert
                  key: job
                  value: 'integrations/kubernetes/eventhandler'
                - action: insert
                  key: loki.resource.labels
                  value: job, cluster
            resource:
              attributes:
                - action: insert
                  key: loki.format
                  value: raw
                - action: insert
                  key: loki.resource.labels
                  value: pod, namespace, container, cluster, filename
            batch:
              timeout: 1s
            memory_limiter:
              limit_mib: 20000
              spike_limit_mib: 500
              check_interval: 5s
          extensions:
            health_check:
              endpoint: 0.0.0.0:13133
          service:
            pipelines:
              traces:
                receivers: [otlp, jaeger]
                processors: [memory_limiter, batch]
                exporters: [otlphttp, otlp]
              logs/k8s_events:
                receivers: [k8s_events]
                processors: [batch, resource/k8s_events]
                exporters: [loki]
              logs:
                receivers: [filelog]
                processors: [resource]
                exporters: [loki]
            telemetry:
              logs:
                encoding: json
            extensions: [health_check]
