# SupDeVinci Equipe 3
![Hackaton 2024 HAKUNA-MATATA](Hackaton_2024_SupDeVinci.png)
## LIEN VERS LA VIDEO DE PRÉSENTATION

<!-- https://youtu.be/Iq6UlUnX_Cg -->
[Vidéo youtube](https://youtu.be/Iq6UlUnX_Cg)

## Documentation
La documentation du projet est disponible dans le répo sous le nom "Documentation_equipe12_repo3.pdf"

## Récupérer le kubeconfig

REQUIS : ```aws cli``` et ```kubectl```

Pour récupérer le kubeconfig il faut exporter les variables AWS contenu dans ce repo en executant les commandes suivantes : 

```shell
$env:AWS_ACCESS_KEY_ID=""

$env:AWS_SECRET_ACCESS_KEY=""

$env:AWS_SESSION_TOKEN=""
```

Ensuite il suffit de récupérer le kubeconfig avec le cli aws avec la commande suivante :

```shell
aws eks --region eu-west-1 update-kubeconfig --name eks-cluster-equipe-03
```

## Infrastructure 

Vous disposez d'un cluster Kubernetes déployé avec le service managé EKS de AmazonWebServices.
Pour déployer les applications un ArgoCD à été installé sur le cluster, ci dessous la liste des outils installés sur le cluster qui vous seront utiles pour le hackaton : 

    - app-grafana
    - app-loki
    - app-mimir
    - app-tempo
    - app-otel

Vous pouvez modifier ces applications.

Les applications suivantes ne doivent absolument pas être modifié :

    - app-argocd
    - argocd-configs
    - infra-apps
    - infra-nfs

Vous pouvez rajoutez d'autres applications si vous le souhaitez via argoCD ou non.

Il y a deux applications qui n'ont pas été déployées avec ArgoCD qui sont :

    - app-pokeshop
    - app-tracetest

Vous n'avez de toute façon pas besoin de toucher à cces applications.

## Url et identifiants de connexion

Les passwords des applications sont enregistrés dans les variables gitlab.

- argocd :
    - url : ab087b8fb83844227aa53aed292d8897-78fe6ae2a780090b.elb.eu-west-1.amazonaws.com
    - user : admin
- grafana :
    - url : a6a43c1b6326d4999acdc28c8417eb27-516ad5efbda8ccde.elb.eu-west-1.amazonaws.com
    - user : admin
- pokeshop :
    - url : adca46ce7d4b54db9bf747b8cd000ed6-1951403598.eu-west-1.elb.amazonaws.com
- tracetest :
    - url : acd2346839e47417fae5ec380faff2ff-1d60e35dd39f0ac1.elb.eu-west-1.amazonaws.com:11633

Pour utilisez Pokeshop avec tracetest vous pouvez trouvez les documentations içi :

    - https://github.com/kubeshop/pokeshop/blob/master/docs/overview.md
    - https://tracetest.io/

Pour l'utilisation de TraceTest et de Pokeshop n'hésitez pas à nous contacter.    

